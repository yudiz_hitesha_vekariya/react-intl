import "./App.css";

import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { Home } from "./components/Pages/Home";
import { AboutUs } from "./components/Pages/AboutUs";
import { ContactUs } from "./components/Pages/ContactUs";
import { NavBar } from "./components/NavBar";

function App() {
  return (
    <>
      <Router>
        <NavBar />
        <div className="pages">
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/about" component={AboutUs} />
            <Route exact path="/contact" component={ContactUs} />
          </Switch>
        </div>
      </Router>
    </>
  );
}

export default App;
