import { LOCALES } from "./locales";

export const messages = {
  [LOCALES.ENGLISH]: {
    learn_to: "Hello, let's learn how to use React-Intl",
    price_display:
      "How {n, number, ::currency/USD} is displayed in your selected language",
    number_display:
      "This is how {n, number} is formatted in the selected locale",
    start_today: "Start Today: {d, date}",
    contact: "please contact_here",
    click_count: "You clicked {count, plural, one {# time} other {# times}}",
    click_button: "Please click the button below",
    click_here: "click here",
    languages: "Languages",
    info: "internationalization capabilities, the React Intl library provides the mechanism to properly translate file text into other languages.",
    syntex: "npm i react-intl",
    deep: " internationalization doesn’t have to be hard, thanks to a new React library. React Intl is an open-source project from Yahoo, and part of Format.js, a collection of JavaScript libraries for internationalization that builds on Javascript’s built-in Intl API.The React Intl library makes internalization in React straightforward, with off-the-shelf components and an API that can handle everything from formatting strings, dates, and numbers, to pluralization.",
  },
  [LOCALES.FRENCH]: {
    learn_to: "Bonjour, apprenons à utiliser React-Intl",
    price_display:
      "Comment {n, number, ::currency/USD} $ s'affiche dans la langue sélectionnée",
    number_display:
      "Voici comment {n, number} sont formatés dans les paramètres régionaux sélectionnés ",
    start_today: "Commencez aujourd'hui: {d, date}",
    contact: "veuillez contacter_ici",
    click_count:
      "Vous avez cliqué {count, plural, one {# fois} other {# fois}}",
    click_button: "Veuillez cliquer sur le bouton ci-dessous",
    click_here: "Cliquez ici",
    languages: "Langues",
    info: "capacités d'internationalisation, la bibliothèque React Intl fournit le mécanisme permettant de traduire correctement le texte du fichier dans d'autres langues.",
    syntex: "npm je réagis-intl",
    deep: "l'internationalisation ne doit pas être difficile, grâce à une nouvelle bibliothèque React. React Intl est un projet open-source de Yahoo et fait partie de Format.js, une collection de bibliothèques JavaScript pour l'internationalisation qui s'appuie sur l'API Intl intégrée de Javascript. La bibliothèque React Intl facilite l'internalisation dans React, avec des des composants d'étagère et une API qui peut tout gérer, du formatage des chaînes, des dates et des nombres à la pluralisation.",
  },
  [LOCALES.GERMAN]: {
    learn_to: "Hallo, lass uns lernen, wie man React-Intl benutzt",
    price_display:
      "Wie {n, number, ::currency/USD} in Ihrer ausgewählten Sprache angezeigt wird",
    number_display:
      "Auf diese Weise werden {n, number} im ausgewählten Gebietsschema formatiert",
    start_today: "Beginnen Sie heute: {d, date}",
    contact: "bitte kontaktieren Sie_hier",
    click_count:
      "Sie haben {count, plural, one {# Mal} other {# Mal}} geklickt",
    click_button: "Bitte klicken Sie auf die Schaltfläche unten",
    click_here: "Klicke hier",
    languages: "Sprachen",
    info: "Internationalisierungsfunktionen bietet die React Intl-Bibliothek den Mechanismus, um Dateitext richtig in andere Sprachen zu übersetzen.",
    syntex: "npm ich reagiere-intl",
    deep: "Internationalisierung muss dank einer neuen React-Bibliothek nicht schwer sein. React Intl ist ein Open-Source-Projekt von Yahoo und Teil von Format.js, einer Sammlung von JavaScript-Bibliotheken für die Internationalisierung, die auf der integrierten Intl-API von Javascript aufbaut. Regalkomponenten und eine API, die alles verarbeiten kann, von der Formatierung von Zeichenfolgen, Datumsangaben und Zahlen bis hin zur Pluralisierung.",
  },
  [LOCALES.JAPANESE]: {
    learn_to: "こんにちは、React-Intlの使い方を学びましょう",
    price_display:
      "選択した言語で{n, number, ::currency/USD}がどのように表示されるか",
    number_display:
      "これは、選択したロケールで{n, number}がフォーマットされる方法です。",
    start_today: "今日から始める：{d, date}",
    contact: "contact_hereに連絡してください",
    click_count: "{count, plural, one {# 回} other {# 回}}クリックしました",
    click_button: "下のボタンをクリックしてください",
    click_here: "ここをクリック",
    languages: "言語",
    info: "国際化機能であるReactIntl​​ライブラリは、ファイルテキストを他の言語に適切に翻訳するメカニズムを提供します。",
    syntex: "npm私は反応します-intl",
    deep: "新しいReactライブラリのおかげで、国際化は難しいことではありません。 React Intlは、Yahooのオープンソースプロジェクトであり、Javascriptの組み込みIntl APIに基づいて構築された国際化用のJavaScriptライブラリのコレクションであるFormat.jsの一部です。ReactIntl​​ライブラリは、Reactの内部化を簡単にします。シェルフコンポーネントと、文字列、日付、数値のフォーマットから複数化まですべてを処理できるAPI。",
  },
};
