import { React, useState } from "react";
import { LOCALES } from "../intl/locales";
import { FormattedMessage } from "react-intl";
import { Content } from "./Content";
import { messages } from "../intl/messages";
import { IntlProvider } from "react-intl";

export const Home = () => {
  const languages = [
    { name: "English", code: LOCALES.ENGLISH },
    { name: "日本語", code: LOCALES.JAPANESE },
    { name: "Français", code: LOCALES.FRENCH },
    { name: "Deutsche", code: LOCALES.GERMAN },
  ];

  const [currentLocale, setCurrentLocale] = useState(getInitialLocal());

  const handleChange = (e) => {
    setCurrentLocale(e.target.value);

    localStorage.setItem("locale", e.target.value);
  };

  function getInitialLocal() {
    const savedLocale = localStorage.getItem("locale");
    return savedLocale || LOCALES.ENGLISH;
  }

  return (
    <IntlProvider
      messages={messages[currentLocale]}
      locale={currentLocale}
      defaultLocale={LOCALES.ENGLISH}
    >
      <div>
        <div className="container header_content">
          <div className="spacer"></div>
          <div className="switcher">
            <FormattedMessage id="languages" />{" "}
            <select onChange={handleChange} value={currentLocale}>
              {languages.map(({ name, code }) => (
                <option key={code} value={code}>
                  {name}
                </option>
              ))}
            </select>
          </div>
        </div>
        <Content />
      </div>
    </IntlProvider>
  );
};
