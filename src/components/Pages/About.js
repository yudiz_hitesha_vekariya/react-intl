import { FormattedMessage } from "react-intl";

export const About = () => {
  return (
    <div className="container hero1">
      <h1 className="title">
        <FormattedMessage id="info" />
      </h1>
      <br></br>
      <p className="syntex">
        <FormattedMessage id="syntex" />
      </p>
      <br></br>
      <img
        src="https://cdn-media-1.freecodecamp.org/images/1*hhTdD39DodXWsVl1OCdEpA.jpeg"
        alt=""
      ></img>
      <br></br>
      <br></br>
      <p className="deep">
        <FormattedMessage id="deep" />
      </p>
    </div>
  );
};
