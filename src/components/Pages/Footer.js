import { useState } from "react";
import { FormattedMessage } from "react-intl";

const Footer = () => {
  const [count, setCount] = useState(0);
  const onChange = () => {
    setCount((prevState) => prevState + 1);
  };

  return (
    <div className="container mt">
      <p className="title">
        <FormattedMessage id="contact" />
      </p>
      <br></br>
      <p>
        <img
          src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSRQjHFTnMbVo8c9nXpVdDofBNmuQvqsPPjqQ&usqp=CAU"
          alt=""
        ></img>
      </p>
      <p>
        <FormattedMessage id="click_button" />
      </p>
      <button onClick={onChange}>
        <FormattedMessage id="click_here" />
      </button>
      <p>
        <FormattedMessage id="click_count" values={{ count: count }} />
      </p>
    </div>
  );
};

export default Footer;
